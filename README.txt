Extra functions:
    1. App supports rotation without reseting the game.
    2. Has overflow menu setting to change the number of cell in grid:
        - Select overflow menu
        - Click on Setting
        - Enter the number of cell per row (e.g: 9 or 12 or any positive integer)
        - Enjoy the game.
