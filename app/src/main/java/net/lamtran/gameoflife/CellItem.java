package net.lamtran.gameoflife;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Lam Tran on 2/21/2017.
 */

class CellItem {
    private boolean isAlive;
    private Pair<Integer, Integer> posInGrid;
    private List<Integer> neighbors;
    private boolean futureState;

    CellItem(Pair<Integer, Integer> posInGrid) {
        this.posInGrid = posInGrid;
    }

    boolean isAlive() {
        return isAlive;
    }

    void setAlive(boolean alive) {
        isAlive = alive;
    }

    void toggleAlive() {
        isAlive = !isAlive;
    }

    public Pair<Integer, Integer> getPosInGrid() {
        return posInGrid;
    }

    public void setPosInGrid(Pair<Integer, Integer> posInGrid) {
        this.posInGrid = posInGrid;
    }

    int getPosInList(int posX, int posY) {
        return posX * GameManager.getInstance().getItemPerRow() + posY;
    }

    int getNeighborAliveCount() {
        int count = 0;
        for (Integer neighbor : getNeighborList()) {
            if (GameManager.getInstance().getItemById(neighbor).isAlive) {
                count++;
            }
        }

        return count;
    }

    private List<Integer> getNeighborList() {
        if (neighbors != null) {
            return neighbors;
        }

        List<Integer> neighbors = new ArrayList<>();
        for (int i = posInGrid.first - 1; i <= posInGrid.first + 1; i++) {
            for (int j = posInGrid.second - 1; j <= posInGrid.second + 1; j++) {
                if (i < 0 || i >= GameManager.getInstance().getItemPerRow() || j < 0 || j >= GameManager.getInstance().getItemPerRow() || (i == posInGrid.first && j == posInGrid.second)) {
                    continue;
                }
                neighbors.add(getPosInList(i, j));
            }
        }
        return neighbors;
    }

    void updateNextStatus() {
        isAlive = futureState;
    }

    boolean setNextStatus() {
        int count = getNeighborAliveCount();
        boolean willAlive = count == 3 || (isAlive && count == 2);
//        System.out.println(" +++ " + count + " --- " + isAlive + " --- " + posInGrid.first + " --- " + posInGrid.second + " --- " + willAlive);
        futureState = willAlive;
        return willAlive;
    }
}
