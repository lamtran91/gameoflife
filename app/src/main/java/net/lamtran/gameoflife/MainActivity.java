package net.lamtran.gameoflife;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    SquareGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = (GridView) findViewById(R.id.mainGridView);
        gridView.setNumColumns(GameManager.getInstance().getItemPerRow());

        adapter = new SquareGridAdapter(this, GameManager.getInstance().getItemList());
        gridView.setAdapter(adapter);

        findViewById(R.id.btn_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.reset_message)
                        .setTitle(R.string.reset_title).setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GameManager.getInstance().resetList();
                        adapter.notifyDataSetChanged();
                    }
                }).setNegativeButton(R.string.btn_camcel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });

        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameManager.getInstance().next();
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.input_popup, (ViewGroup) findViewById(android.R.id.content), false);
        final EditText input = (EditText) viewInflated.findViewById(R.id.text_input_popup);

        switch (item.getItemId()) {
            case R.id.menu_setting:
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.choose_size_message)
                        .setView(viewInflated)
                        .setTitle(R.string.choose_size_title).setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        GameManager.getInstance().setItemPerRow(Integer.parseInt(input.getText().toString()));
                        gridView.setNumColumns(GameManager.getInstance().getItemPerRow());
                        ((SquareGridAdapter) gridView.getAdapter()).setItems(GameManager.getInstance().getItemList());
                    }
                }).setNegativeButton(R.string.btn_camcel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}