package net.lamtran.gameoflife;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lam Tran on 2/22/2017.
 */
class GameManager {
    private List<CellItem> mItems;
    private int mItemPerRow = 12;

    private static GameManager ourInstance = new GameManager();

    static GameManager getInstance() {
        return ourInstance;
    }

    private GameManager() {
    }

    void setItemPerRow(int itemPerRow) {
        mItemPerRow = itemPerRow;
    }

    int getItemPerRow() {
        return mItemPerRow;
    }

    List<CellItem> getItemList() {
        if (mItems != null && mItems.size() == Math.pow(mItemPerRow, 2)) {
            return mItems;
        }

        mItems = new ArrayList<>();
        for (int i = 0; i < mItemPerRow; i++) {
            for (int j = 0; j < mItemPerRow; j++) {
                mItems.add(new CellItem(new Pair<>(i, j)));
            }
        }

        return mItems;
    }

    void resetList() {
        for (CellItem item : mItems) {
            item.setAlive(false);
        }
    }

    void next() {
        for (CellItem item : mItems) {
            item.setNextStatus();
        }

        for (CellItem item : mItems) {
            item.updateNextStatus();
        }
    }

    CellItem getItemById(int id) {
        return mItems.get(id);
    }
}
