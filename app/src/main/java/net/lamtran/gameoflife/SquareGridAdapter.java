package net.lamtran.gameoflife;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import static android.R.color.white;

/**
 * @author Lam Tran on 2/21/17.
 */
class SquareGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<CellItem> mItems;

    SquareGridAdapter(Context context, List<CellItem> items) {
        mContext = context;
        mItems = items;
    }

    void setItems(List<CellItem> items) {
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CellItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.square_grid_view, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CellItem item = mItems.get(position);

        viewHolder.gridItemView.setBackgroundResource(item.isAlive() ? R.drawable.circle_red_with_background : white);
        convertView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    toggleItem(position, v);
                }

                return true;
            }
        });
        return convertView;
    }

    private class ViewHolder {
        SquareGridItemView gridItemView;

        ViewHolder(View view) {
            gridItemView = (SquareGridItemView) view.findViewById(R.id.gridItemView);
        }
    }

    private void toggleItem(int pos, View view) {
        CellItem item = mItems.get(pos);
        item.toggleAlive();
        ((ViewHolder) view.getTag()).gridItemView.setBackgroundResource(item.isAlive() ? R.drawable.circle_red_with_background : white);
    }
}

