package net.lamtran.gameoflife;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

/**
 * @author Lam Tran on 2/21/17.
 */
public class SquareGridItemView extends View {
    public SquareGridItemView(Context context) {
        super(context);
    }

    public SquareGridItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareGridItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}
