package net.lamtran.gameoflife;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * @author Lam Tran on 2/21/17.
 */
public class SquareGridView extends GridView {
    public SquareGridView(Context context) {
        super(context);
    }

    public SquareGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}
